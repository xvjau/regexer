/*
 * <B1tF0rge> 2014 Todos os direitos reservados
 */

#pragma once

#include <QtCore/QAbstractTableModel>

#include <boost/regex.hpp>

#include <vector>

class RegExModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    RegExModel(boost::regex _regex, QStringList _data, QObject* _parent = nullptr);

    virtual QVariant data(const QModelIndex& _index, int _role) const override;
    virtual int columnCount(const QModelIndex& _parent) const override;
    virtual int rowCount(const QModelIndex& _parent) const override;

private:
    struct Row
    {
        bool isMatch = false;
        std::vector<QVariant> matches;
    };
    std::vector<Row> m_rows;

    int m_colCount = 0;
};
