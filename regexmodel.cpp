/*
 * <B1tF0rge> 2014 Todos os direitos reservados
 */

#include "regexmodel.h"

RegExModel::RegExModel(boost::regex _regex, QStringList _data, QObject* _parent):
    QAbstractTableModel(_parent)
{
    for(auto str : _data)
    {
        int col = 0;
        Row row;
        auto s = str.toStdString();

//         auto it = boost::sregex_iterator(s.begin(), s.end(), _regex);
//         auto end = boost::sregex_iterator();
//
//         if (it != end)
//         {
//             row.isMatch = true;
//
//             for(; it != end; it++, col++)
//             {
//                 row.matches.push_back(QString::fromStdString(it->str()));
//             }
//         }
//         else
//         {
//             row.isMatch = false;
//             col++;
//
//             row.matches.push_back(str);
//         }

        boost::smatch what;
        if (boost::regex_match(s, what, _regex))
        {
            for (std::size_t i = 0; i < what.size(); i++, col++)
            {
                row.matches.push_back(QString::fromStdString(what[i]));
            }
        }
        else
        {
            row.isMatch = false;
            col++;

            row.matches.push_back(str);
        }

        m_rows.push_back(std::move(row));

        if (col > m_colCount)
            m_colCount = col;
    }
}


QVariant RegExModel::data(const QModelIndex& _index, int _role) const
{
    switch(_role)
    {
        case Qt::DisplayRole:
        {
            const Row& row = m_rows[_index.row()];
            if ((unsigned int)_index.column() < row.matches.size())
            {
                return row.matches[_index.column()];
            }
            break;
        }
    }

    return QVariant();
}

int RegExModel::columnCount(const QModelIndex&) const
{
    return m_colCount;
}

int RegExModel::rowCount(const QModelIndex&) const
{
    return m_rows.size();
}
