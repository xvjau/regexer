
#include <QtWidgets/QApplication>

#include "RegExer.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    app.setOrganizationName("B1tF0rge");
    app.setOrganizationDomain("bitforge.com.br");
    app.setApplicationName("RegExer");
    app.setApplicationDisplayName("Regular Expressioner");

    RegExer regexer;
    regexer.show();

    return app.exec();
}
