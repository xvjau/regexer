#include "RegExer.h"

#include "ui_RegExer.h"

#include <boost/regex.hpp>

#include <QtCore/QTextStream>
#include <QtCore/QFileInfo>
#include <QtCore/QSettings>

#include <QtWidgets/QFileDialog>
#include <apr-1/apr_poll.h>

RegExer::RegExer()
{
    m_ui = new Ui::RegExer();
    m_ui->setupUi(this);

    m_ui->tvResult->setVisible(false);

    connect(m_ui->teRexEx, SIGNAL(textChanged()), this, SLOT(regexChanged()));
    connect(m_ui->teSample, SIGNAL(textChanged()), this, SLOT(regexChanged()));

    connect(m_ui->acOpenSample, SIGNAL(triggered()), this, SLOT(openSample()));
}

RegExer::~RegExer()
{
}

inline QTextStream& operator<<(QTextStream &_str, const std::string &_text)
{
    _str << QString::fromStdString(_text);
    return _str;
}

void RegExer::regexChanged()
{
    auto regexStr = m_ui->teRexEx->toPlainText();

    boost::regex regex;

    try
    {
        regex = boost::regex(regexStr.toStdString());
    }
    catch(const boost::regex_error& e)
    {
        m_ui->tvResult->setVisible(false);
        m_ui->teResult->setVisible(true);

        m_ui->teResult->setPlainText(e.what());
        return;
    }

    m_ui->teResult->clear();

    m_ui->tvResult->setVisible(true);
    m_ui->teResult->setVisible(false);

    auto sample = m_ui->teSample->toPlainText().split("\n");

    m_model.reset(new RegExModel(std::move(regex), std::move(sample)));
    m_ui->tvResult->setModel(m_model.get());
}

void RegExer::openSample()
{
    QSettings settings;


    settings.beginGroup("Sample");

    QString dir;
    if (settings.contains("last_dir"))
        dir = settings.value("last_dir").toString();

    auto fileName = QFileDialog::getOpenFileName(this, tr("Sample File"), dir);
    if (!fileName.isEmpty())
    {
        QFileInfo info(fileName);
        settings.setValue("last_dir", info.absolutePath());

        QFile file(fileName);

        file.open(QIODevice::ReadOnly);

        QTextStream str(&file);
        m_ui->teSample->setPlainText(str.readAll());
    }
}
