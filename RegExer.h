#pragma once

#include <QtWidgets/QMainWindow>
#include <memory>
#include "regexmodel.h"

namespace Ui {
class RegExer;
}

class RegExModel;

class RegExer : public QMainWindow
{
    Q_OBJECT
public:
    RegExer();
    virtual ~RegExer();

protected:
    Ui::RegExer *m_ui = nullptr;

    std::unique_ptr<RegExModel> m_model;

protected slots:
    void regexChanged();
    void openSample();
};
